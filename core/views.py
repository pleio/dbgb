# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import Http404, HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import render
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .models import Data, Consumer, DataInfo, ClusterInfo, DataConsumerInfo
from .forms import FilterForm, ConsumerEditForm, ConsumerDataEditForm
import reversion
import datetime

now = datetime.datetime.now()

@login_required
def home(request):
    return render(request, 'home.html')

@login_required
def data(request):
    if request.GET:
        filter_form = FilterForm(request.GET)
        if filter_form.is_valid():
            filter_data = filter_form.cleaned_data

            request.session['filter_q'] = filter_data.get('q')
            request.session['filter_cluster'] = filter_data.get('cluster')
            request.session['filter_year'] = filter_data.get('year')
    else:
        filter_form = FilterForm()
        filter_form.fields['q'].initial = request.session.get('filter_q', '')
        filter_form.fields['cluster'].initial = request.session.get('filter_cluster', '')
        filter_form.fields['year'].initial = request.session.get('filter_year', '%i' % now.year)

    # Get data in filter selection
    data_selection = DataInfo.objects.all().order_by('data__name')
    data_selection = data_selection.filter(year=request.session.get('filter_year', '%i' % now.year))
    cluster_info = None

    if request.session.get('filter_cluster', None):
        data_selection = data_selection.filter(data__cluster__name=request.session.get('filter_cluster', ''))
        cluster_info = ClusterInfo.objects.filter(year=request.session.get('filter_year', '%i' % now.year), cluster__name=request.session.get('filter_cluster', '')).first()
    if request.session.get('filter_q', None):
        search_text = request.session.get('filter_q', '')
        data_selection = data_selection.filter(Q(data__name__icontains=search_text) | Q(information__icontains=search_text) | Q(value_range__icontains=search_text) | Q(value_conditions__contains=search_text))

    context = {
        'filter_form': filter_form,
        'data_selection': data_selection,
        'cluster_info': cluster_info,
        'sidebar': True
    }

    return render(request, 'data_search.html', context)

@login_required
def data_detail(request, id=None):
    try:
        data = Data.objects.get(id=id)
    except Data.DoesNotExist:
        raise Http404("Consumer does not exist")

    year = request.GET.get('year', request.session.get('filter_year', '%i' % now.year))

    context = {
        'data': data,
        'data_info': data.info.filter(year=year).first(),
        'data_years': map(lambda x: str(x.year), data.info.all()),
        'consumers': data.consumers.exclude(use__id=99).exclude(use__id=7).exclude(consumer__year__gt=year),
        'selected_consumer': request.session.get('last_consumer', None),
        'selected_year': year
    }

    return render(request, 'data.html', context)

@login_required
def consumer_detail(request, id):
    try:
        consumer = Consumer.objects.get(id=id)
    except Consumer.DoesNotExist:
        raise Http404("Consumer does not exist")

    request.session['last_consumer'] = id

    context = {
        'consumer': consumer,
        'data': consumer.data.exclude(use__id=99).exclude(use__id=7),
    }

    return render(request, 'consumer.html', context)

@login_required
def manage_consumer_data(request, id):
    try:
        consumer = Consumer.objects.get(id=id, users__user=request.user)
    except Consumer.DoesNotExist:
        raise Http404("Consumer does not exist")

    context = {
        'consumer': consumer,
        'data': consumer.data.exclude(use__id=7).exclude(use__id=99), # exclude 7 = Geen gebruik
        'new_count': consumer.data.filter(use__id=99).count()
    }

    return render(request, 'manage_consumer_data.html', context)

@login_required
def manage_consumer_data_new(request, id):
    try:
        consumer = Consumer.objects.get(id=id, users__user=request.user)
    except Consumer.DoesNotExist:
        raise Http404("Consumer does not exist")

    context = {
        'consumer': consumer,
        'data': consumer.data.filter(use__id=99), # exclude 7 = Geen gebruik
        'new_count': consumer.data.filter(use__id=99).count()
    }

    return render(request, 'manage_consumer_data_new.html', context)

@login_required
def manage_consumer_data_detail(request, id, data_id):
    try:
        consumer = Consumer.objects.get(id=id, users__user=request.user)
    except Consumer.DoesNotExist:
        raise Http404("Consumer does not exist")

    try:
        data_consumer = DataConsumerInfo.objects.get(id=data_id)
    except DataConsumerInfo.DoesNotExist:
        raise Http404("Data does not exist")

    if request.POST:
        form = ConsumerDataEditForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data

            with reversion.create_revision():
                changed = []

                if data_consumer.use != data['use']:
                    changed.append('use')
                    data_consumer.use = data['use']

                if data_consumer.use_text != data['use_text']:
                    changed.append('use_text')
                    data_consumer.use_text = data['use_text']

                if data_consumer.consequence_missing != data['consequence_missing']:
                    changed.append('consequence_missing')
                    data_consumer.consequence_missing = data['consequence_missing']

                if data_consumer.consequence_missing_text != data['consequence_missing_text']:
                    data_consumer.consequence_missing_text = data['consequence_missing_text']
                    changed.append('consequence_missing_text')

                if data_consumer.basis_use != data['basis_use']:
                    data_consumer.basis_use = data['basis_use']
                    changed.append('basis_use')

                data_consumer.save()

                # Store some meta-information.
                changed_text = ', '.join(changed)
                reversion.set_user(request.user)
                reversion.set_comment("Via web: %s gewijzigd" % (changed_text))

                messages.add_message(request, messages.INFO, 'Wijzigingen voor %s succesvol opgeslagen.' % consumer.name)

            return HttpResponseRedirect(reverse('manage_consumer_data', args=[id]))
    else:
        form = ConsumerDataEditForm(initial={
            'use': data_consumer.use,
            'use_text': data_consumer.use_text,
            'consequence_missing': data_consumer.consequence_missing,
            'consequence_missing_text': data_consumer.consequence_missing_text,
            'basis_use': data_consumer.basis_use
        })
        
    context = {
        'consumer': consumer,
        'form': form,
        'data': data_consumer,
        'new_count': consumer.data.filter(use__id=99).count()
    }

    return render(request, 'manage_consumer_data_detail.html', context)

@login_required
def manage_consumer(request, id):
    try:
        consumer = Consumer.objects.get(id=id, users__user=request.user)
    except Consumer.DoesNotExist:
        raise Http404("Consumer does not exist")

    if request.POST:
        form = ConsumerEditForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.cleaned_data

            with reversion.create_revision():
                changed = []

                if consumer.fullname != data['fullname']:
                    changed.append('fullname')
                    consumer.fullname = data['fullname']

                if consumer.description != data['description']:
                    changed.append('description')
                    consumer.description = data['description']

                if consumer.website != data['website']:
                    changed.append('website')
                    consumer.website = data['website']

                if data['logo']:
                    changed.append('logo')
                    consumer.logo = data['logo']

                consumer.save()

                # Store some meta-information.
                changed_text = ', '.join(changed)
                reversion.set_user(request.user)
                reversion.set_comment("Via web: %s gewijzigd" % (changed_text))

                messages.add_message(request, messages.INFO, 'Wijzigingen voor %s succesvol opgeslagen.' % consumer.name)

            return HttpResponseRedirect(reverse('manage_consumer', args=[id]))

    else:
        form = ConsumerEditForm(initial={
            'fullname': consumer.fullname,
            'description': consumer.description,
            'website': consumer.website
        })

    context = {
        'consumer': consumer,
        'form': form,
        'new_count': consumer.data.filter(use__id=99).count()
    }

    return render(request, 'manage_consumer.html', context)

@login_required
def consumer(request):
    context = {
        'consumers': Consumer.objects.all()
    }
    return render(request, 'consumer_list.html', context)

@login_required
def documents(request):
    context = {
    }
    return render(request, 'documents.html', context)

@login_required
def rules(request):
    return render(request, 'rules.html')

@login_required
def account(request):
    context = {
        'consumers': Consumer.objects.filter(users__user=request.user).all()
    }
    return render(request, 'account.html', context)


import logging

from django.db.models.signals import post_save
from django.dispatch import receiver

from core.models import Consumer

logger = logging.getLogger(__name__)

@receiver(post_save, sender=Consumer)
def handle_new_consumer(sender, **kwargs):
    if kwargs.get('raw'):
        return
    consumer = kwargs.get('instance')
    consumer.update_relations()

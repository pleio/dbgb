import re

from docx import Document
from docx.document import Document as _Document
from docx.oxml.text.paragraph import CT_P
from docx.oxml.table import CT_Tbl
from docx.table import _Cell, Table
from docx.text.paragraph import Paragraph

from django.core.management.base import BaseCommand, CommandError
from core.models import Data, DataInfo, Consumer, DataConsumerInfo, Cluster, ClusterInfo

class Command(BaseCommand):
    help = 'Import Data from DOCX'

    def add_arguments(self, parser):
        parser.add_argument('filename', nargs=1, type=str)
        parser.add_argument('year', nargs=1, type=int)

    def handle(self, *args, **options):
        filename = options['filename'][0]
        year = options['year'][0]

        document = Document(filename)

        list = self.parse_docx(document)

        for item in list:
            name = item["name"]
            parsed = self.parse_data(item["text"])
            parsed_cluster = self.parse_cluster(item["cluster_text"])

            cluster, cluster_created = Cluster.objects.update_or_create(
                name=item["cluster"]
            )
            self.stdout.write(self.style.SUCCESS('%s cluster %s' % (item["cluster"], "inserted" if cluster_created else "updated")))

            cluster_info, cluster_info_created = ClusterInfo.objects.update_or_create(
                cluster=cluster,
                year=int(year),
                defaults={
                    'information': parsed_cluster["info"],
                    'conditions': parsed_cluster["condities"]
                }
            )

            self.stdout.write(self.style.SUCCESS('%s cluster-info %s' % (item["cluster"], "inserted" if cluster_info_created else "updated")))


            data, data_created = Data.objects.update_or_create(
                name=name,
                defaults={
                    'description': parsed["description"],
                    'identification': parsed["id"],
                    'cluster': cluster
                }
            )
            self.stdout.write(self.style.SUCCESS('%s base %s' % (name, "inserted" if data_created else "updated")))

            info, info_created = DataInfo.objects.update_or_create(
                data=data,
                year=int(year),
                defaults={
                    'format': parsed["format"],
                    'information': parsed["info"],
                    'value_range': parsed["waardebereik"],
                    'value_conditions': parsed["condities"]
                }
            )

            self.stdout.write(self.style.SUCCESS('%s info %s' % (name, "inserted" if info_created else "updated")))

        # update consumers with new data
        consumers = Consumer.objects.all()

        for consumer in consumers:
            consumer.update_relations()

    def parse_data(self, text):
        parsed = {
            "id": self.getText(r"^Identificatie:([^\n]*)", text),
            "format": self.getText(r"^Formaat:([^\n]*)", text),
            "description": self.getText(r"^Omschrijving:([^\n]*)", text),
            "waardebereik": self.getText(r"^Waardenbereik:(.*?)\n(Condities|Poortcontrole|Toelichting|Terugkoppel)", text),
            "condities": self.getText(r"^((Terugkoppelcontrole|Conditie|Poortcontrole)(((?!Toelichting).)+))", text),
            "info": self.getText(r"^Toelichting:\n(.*)$", text),
        }

        return parsed

    def parse_cluster(self, text):
        parsed = {
            "info": self.getText(r"^Toelichting voor de \(gehele\) gegevensgroep:\n(.*)$", text),
            "condities": self.getText(r"^Condities voor de gehele gegevensgroep:\n(((?!Toelichting).)+)", text)
        }

        return parsed

    def getText(self, regex, text):
        match = re.search(regex, text, re.MULTILINE|re.DOTALL)
        if match:
            return match.group(1).strip()
        else:
            return ""

    def parse_docx(self, document):
        section_current = {
            "name": None,
            "text": "",
            "cluster": "",
            "cluster_text": ""
        }
        start_grabbing = None
        result = []

        for block in iter_block_items(document):

            if isinstance(block, Paragraph) and block.style.name.startswith('Heading') and block.text.strip() == "Werknemersgegevens":
                start_grabbing = True

            if not start_grabbing:
                continue

            if isinstance(block, Paragraph) and block.style.name == 'Heading 5' and block.text.strip() != '':
                if section_current["name"]:
                    result.append(section_current.copy())
                section_current["name"] = block.text.strip()
                section_current["text"] = ""
            elif block.style.name.startswith('Heading') or block.style.name.startswith('kop'):
                if section_current["name"]:
                    result.append(section_current.copy())
                section_current["name"] = None
                section_current["text"] = ""
                if (block.style.name == 'Heading 2' or block.style.name == 'kop 2') and block.text.strip() != '':
                    section_current["cluster"] = block.text.strip()
                    section_current["cluster_text"] = ""

            if section_current["name"]:
                section_current["text"] += block.text + "\n" if isinstance(block, Paragraph) else '<table>'
                if not isinstance(block, Paragraph):
                    self.stdout.write('Table found in: %s' % section_current["name"])
            elif section_current["cluster"]:
                section_current["cluster_text"] += block.text + "\n" if isinstance(block, Paragraph) else '<table>'

        return result

def iter_block_items(parent):
    """
    Generate a reference to each paragraph and table child within *parent*,
    in document order. Each returned value is an instance of either Table or
    Paragraph. *parent* would most commonly be a reference to a main
    Document object, but also works for a _Cell object, which itself can
    contain paragraphs and tables.
    """
    if isinstance(parent, _Document):
        parent_elm = parent.element.body
        # print(parent_elm.xml)
    elif isinstance(parent, _Cell):
        parent_elm = parent._tc
    else:
        raise ValueError("something's not right")

    for child in parent_elm.iterchildren():
        if isinstance(child, CT_P):
            yield Paragraph(child, parent)
        elif isinstance(child, CT_Tbl):
            yield Table(child, parent)
        # else:
        #     print(child)

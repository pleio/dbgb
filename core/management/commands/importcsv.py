from django.core.management.base import BaseCommand, CommandError
from core.models import Data, DataInfo, Consumer, DataConsumerInfo, Use, ConsequenceMissing
import csv

class Command(BaseCommand):
    help = 'Import Data from CSV'

    def add_arguments(self, parser):
        parser.add_argument('filename', nargs=1, type=str)

    def handle(self, *args, **options):
        filename = options['filename'][0]

        with open(filename, newline='', encoding='utf-8-sig') as csvfile:
            reader = csv.DictReader(csvfile)
            if reader.fieldnames[0] == 'IA_Gegeven' and reader.fieldnames[2] == 'IA_Cluster':
                self.stdout.write(self.style.SUCCESS('Detected DataInfo file'))
                self.read_data(reader)
            elif reader.fieldnames[0] == 'IS_Gebruiker' and reader.fieldnames[1] == 'IS_Specificatie':
                self.stdout.write(self.style.SUCCESS('Detected Consumer file'))
                self.read_consumer(reader)
            elif reader.fieldnames[0] == 'IA_Gegeven' and reader.fieldnames[1] == 'IG_Gebruiker':
                self.stdout.write(self.style.SUCCESS('Detected DataConsumerInfo file'))
                self.read_data_consumer_info(reader)
            else:
                self.stdout.write(self.style.ERROR('Unregonized file'))

        self.stdout.write(self.style.SUCCESS('Import finished'))

    def read_data(self, reader):
        for row in reader:
            data, data_created = Data.objects.get_or_create(
                name=row['IA_Gegeven'],
                cluster=row['IA_Cluster'],
                description=row['IA_Toelichting']
            )
            self.stdout.write(self.style.SUCCESS('- %s %s' % (row['IA_Gegeven'], data_created)))

            for affix in ['', '1', '2', '3', '4', '5', '6']:
                if row['IA_JaarT'+affix]:
                    DataInfo.objects.get_or_create(
                        data=data,
                        year=int(row['IA_JaarT'+affix]),
                        information=row['IA_ToelichtingT'+affix],
                        value_range=row['IA_WaardenbereikT'+affix],
                        value_conditions=row['IA_ConditiesT'+affix]
                    )

    def read_consumer(self, reader):
        for row in reader:
            consumer, consumer_created = Consumer.objects.get_or_create(
                name=row['IS_Gebruiker'],
                description=row['IS_Specificatie'],
                website=row['Website']
            )

            # if exists update metadata
            consumer.description = row['IS_Specificatie']
            consumer.website = row['Website']
            consumer.save()

            self.stdout.write(self.style.SUCCESS('- %s %s' % (row['IS_Gebruiker'], consumer_created)))

    def read_data_consumer_info(self, reader):
        for row in reader:
            data = Data.objects.filter(name__iexact=row['IA_Gegeven']).first()
            if not data:
                self.stdout.write(self.style.ERROR('Data "%s" not found!' % row['IA_Gegeven']))
            else:
                consumer = Consumer.objects.filter(name__iexact=row['IG_Gebruiker']).first()
                if not consumer:
                    self.stdout.write(self.style.ERROR('Consumer "%s" not found!' % row['IG_Gebruiker']))

                if row['IG_CodeA']:
                    use = Use.objects.filter(id=row['IG_CodeA']).first()
                else:
                    use = Use.objects.get(id=99)

                if row['IG_CodeB']:
                    consequence_missing = ConsequenceMissing.objects.filter(id=row['IG_CodeB']).first()
                else:
                    consequence_missing = None

                data_consumer_info, data_consumer_info_inserted = DataConsumerInfo.objects.get_or_create(
                    data=data,
                    consumer=consumer,
                    use=use,
                    use_text=row['IG_OmschrijvingA'],
                    consequence_missing=consequence_missing,
                    consequence_missing_text=row['IG_OmschrijvingB'],
                    basis_use=row['IG_GrondslagAfname']
                )

                if not data_consumer_info_inserted:
                    self.stdout.write(self.style.ERROR('Data for "%s" - "%s" already exists' % (data.name, consumer.name)))

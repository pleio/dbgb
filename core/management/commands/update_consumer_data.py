from django.core.management.base import BaseCommand, CommandError
from core.models import Consumer

class Command(BaseCommand):
    help = 'Update consumer data relations'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        consumers = Consumer.objects.all()

        for consumer in consumers:
            consumer.update_relations()
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from import_export import resources
from import_export import fields
from import_export.admin import ExportMixin, ImportMixin
from import_export.widgets import ForeignKeyWidget
from import_export.results import RowResult
from .models import Consumer, Data, DataInfo, DataConsumerInfo, Use, Delivery, ConsumerUser, EventLog
from .filters import SingleTextInputFilter
from reversion.admin import VersionAdmin


class DataInfoAdmin(admin.ModelAdmin):
    list_filter = ('year', )

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request, obj=None):
        return False

class ConsumerUserAdmin(admin.ModelAdmin):
    list_display = ('user', 'consumer',)

    list_filter = ('consumer__name', )


class DataInfoInline(admin.StackedInline):
    model = DataInfo
    extra = 1

class DataAdmin(admin.ModelAdmin):
    inlines = [
        DataInfoInline,
    ]

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_add_permission(self, request, obj=None):
        return False

class ConsumerAdmin(VersionAdmin):
    list_display = ('name', 'fullname', 'relation_number')

class DataConsumerInfoResouce(resources.ModelResource):
    class Meta:
        model = DataConsumerInfo
        fields = ('id', 'consumer__name', 'data__name', 'use__name', 'use_text', 'consequence_missing__name', 'consequence_missing_text', 'basis_use')
        export_order = ('id', 'consumer__name', 'data__name', 'use__name', 'use_text', 'consequence_missing__name', 'consequence_missing_text', 'basis_use')

def make_use_actions(use):
    def assign_to_use(modeladmin, request, queryset):
        queryset.update(use=use)

    assign_to_use.short_description = "Pas gebruik aan naar: {0} ".format(use.name)
    assign_to_use.__name__ = "assign_to_use_{0}".format(use.id)

    return assign_to_use

class DataConsumerInfoAdmin(ExportMixin, VersionAdmin):
    list_display = ('data', 'consumer', 'use', 'consequence_missing')
    list_filter = ('consumer__name', 'data__info__year', 'use__name',)
    resource_class = DataConsumerInfoResouce
    actions = ['set_used', 'set_not_used']

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']

        for use in Use.objects.all():
            action = make_use_actions(use)
            actions[action.__name__] = (action, action.__name__, action.short_description)

        return actions

    def has_add_permission(self, request, obj=None):
        return False


class DeliveryResource(resources.ModelResource):
    # match: 'Naam afnemer', 'Relatienummer ', 'Contractsoort ', 'Contractnummer', 'Soort levering', 'Frequentie'
    contract_number = fields.Field(column_name="Contractnummer", attribute="contract_number")
    contract_type = fields.Field(column_name="Contractsoort", attribute="contract_type")
    delivery_type = fields.Field(column_name="Soort levering", attribute="delivery_type")
    frequency = fields.Field(column_name="Frequentie", attribute="frequency")

    consumer = fields.Field(
        column_name="Relatienummer",
        attribute="consumer",
        widget=ForeignKeyWidget(Consumer, 'relation_number')
    )

    class Meta:
        model = Delivery
        import_id_fields = ('consumer', 'contract_number')
        fields = ('contract_number', 'contract_type', 'frequency', 'delivery_type')
        #skip_unchanged = True
        report_skipped = True
        raise_errors = False

    def import_row(self, row, instance_loader, **kwargs):
        # overriding import_row to ignore errors and skip rows that fail to import
        # without failing the entire import
        import_result = super(DeliveryResource, self).import_row(row, instance_loader, **kwargs)
        if import_result.import_type == RowResult.IMPORT_TYPE_ERROR:
            # Copy the values to display in the preview report
            import_result.diff = [row.get('Contractnummer') or '', row.get('Contractsoort') or '', row.get('Soort levering') or '', row.get('Frequentie') or '', row.get('Relatienummer') or '']
            # Add a column with the error message
            import_result.diff.append('Errors: {}'.format([err.error for err in import_result.errors]))
            # clear errors and mark the record to skip
            import_result.errors = []
            import_result.import_type = RowResult.IMPORT_TYPE_SKIP

        return import_result

class DeliveryAdmin(ImportMixin, admin.ModelAdmin):
    list_display = ('consumer', 'contract_type', 'contract_number', 'delivery_type', 'frequency')
    list_filter = ('consumer',)
    resource_class = DeliveryResource


class EventLogAdmin(admin.ModelAdmin):
    class UserIdFilter(SingleTextInputFilter):
        title = _("User Id")
        parameter_name = "userid"

        def queryset(self, request, queryset):
            if self.value():
                return queryset.filter(userid__iexact=self.value())


    class EmailFilter(SingleTextInputFilter):
        title = _("Email address")
        parameter_name = "email"

        def queryset(self, request, queryset):
            if self.value():
                return queryset.filter(email__iexact=self.value())


    class IPFilter(SingleTextInputFilter):
        title = _("IP address")
        parameter_name = "ip"

        def queryset(self, request, queryset):
            if self.value():
                return queryset.filter(ip__iexact=self.value())


    actions = None
    search_fields = []
    readonly_fields = ("id", "userid", "email", "ip", "event_type", "data", "created")
    ordering = ("-created",)
    list_filter = [
        UserIdFilter,
        EmailFilter,
        IPFilter,
        "event_type",
    ]
    list_display_links = None
    list_display = ("userid", "email", "ip", "event_type", "data", "created")


# Register your models here.
admin.site.register(Data, DataAdmin)
admin.site.register(DataInfo, DataInfoAdmin)
admin.site.register(Consumer, ConsumerAdmin)
admin.site.register(DataConsumerInfo, DataConsumerInfoAdmin)
admin.site.register(ConsumerUser, ConsumerUserAdmin)
#admin.site.register(ConsequenceMissing)
admin.site.register(Delivery, DeliveryAdmin)
admin.site.register(EventLog, EventLogAdmin)

admin.site.site_header = "DBGB Beheer"

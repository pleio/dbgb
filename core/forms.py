# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime

from django.conf import settings
from django.forms import Form, CharField, ModelChoiceField, TextInput, Select, ChoiceField, Textarea, URLField, ImageField, FileInput, ModelChoiceField
from .models import Data, DataInfo, Cluster, Use, ConsequenceMissing

def unique_clusters():
    return [("", "---------")] + list(Cluster.objects.values_list('name', 'name').order_by('name').distinct())

def years():
    now = datetime.datetime.now()
    current = now.year
    years = list(range(current - 3, current+1))
    if settings.ENV == "TEST":
        years.append(current+1)
    
    year_list = []
    for year in years:
        year_list.append((f"{year}", f"{year}"))

    return year_list


class FilterForm(Form):
    q = CharField(max_length=100, required=False, label='Zoekterm', widget=TextInput(attrs={'class': 'form-control'}))
    cluster = ChoiceField(choices=unique_clusters, required=False, label='Cluster', widget=Select(attrs={'class': 'form-control'}))
    year = ChoiceField(choices=years, required=True, label='Jaar', widget=Select(attrs={'class': 'form-control'}))

class ConsumerEditForm(Form):
    fullname = CharField(
        max_length=140,
        required=False,
        widget=TextInput(attrs={'class': 'form-control'}),
        label='Volledige naam'
    )
    description = CharField(
        required=False,
        widget=Textarea(attrs={'class': 'form-control'}),
        label='Informatie'
    )
    website = URLField(
        required=False,
        widget=TextInput(attrs={'class': 'form-control'}),
        label='Website',
        help_text='Let goed op dat de link start met http:// of https:// '
    )
    logo = ImageField(
        required=False,
        widget=FileInput(attrs={
            'class': 'custom-file-input',
            'accept': 'image/gif, image/jpeg, image/png',
            'd-update-image': 'consumer_logo',
        }),
        label='Logo'
    )


class ConsumerDataEditForm(Form):
    use = ModelChoiceField(
        required=True,
        queryset=Use.objects.all().exclude(id=7),
        label='Aanwending'
    )

    use_text = CharField(
        required=False,
        widget=Textarea(attrs={'class': 'form-control'}),
        label='Toelichting aanwending'
    )

    consequence_missing = ModelChoiceField(
        required=False,
        queryset=ConsequenceMissing.objects.all(),
        label='Gevolg ontbreken'
    )

    consequence_missing_text = CharField(
        required=False,
        widget=Textarea(attrs={'class': 'form-control'}),
        label='Toelichting gevolg ontbreken'
    )

    basis_use = CharField(
        required=False,
        widget=Textarea(attrs={'class': 'form-control'}),
        label='Grondslag afname gebruik'
    )

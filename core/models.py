# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth import get_user_model
import reversion
from datetime import datetime
from django.utils.translation import gettext_lazy as _

User = get_user_model()

def current_year():
    return datetime.now().year

class Cluster(models.Model):
    """Clustering data."""

    class Meta:
        ordering = ['name']

    name = models.CharField(max_length=140, unique=True)

    def __str__(self):
        return self.name

class ClusterInfo(models.Model):
    """Cluster information for each year."""

    class Meta:
        ordering = ['-year']

    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE, related_name='info')
    year = models.PositiveSmallIntegerField()
    information = models.TextField()
    conditions = models.TextField()

    def __str__(self):
        return '%s (%s)' % (self.data.name, self.year)

@reversion.register()
class Consumer(models.Model):
    """Consumer of data."""

    class Meta:
        ordering = ['fullname', 'name']
        verbose_name = 'afnemer'
        verbose_name_plural = 'afnemers'

    name = models.CharField(max_length=140, verbose_name="naam")
    fullname = models.CharField(default='', max_length=140, blank=True)
    description = models.TextField(verbose_name="omschrijving", blank=True)
    website = models.URLField(default='', blank=True)
    logo = models.ImageField(upload_to='logos', null=True, blank=True)
    year = models.PositiveSmallIntegerField(default=current_year, verbose_name="vanaf jaar")
    relation_number = models.IntegerField(null=True, blank=True, verbose_name="relatienummer")

    def __str__(self):
        if self.fullname:
            return self.fullname
        return self.name

    # Auto create default consumer info
    def update_relations(self):
        items = Data.objects.exclude(consumers__consumer=self)
        use = Use.objects.get(name="Onbekende code/aanpassen!")
        for data in items:
            DataConsumerInfo.objects.create(data=data, consumer=self, use=use)
    
class Data(models.Model):
    """Data base model."""

    class Meta:
        ordering = ['name']
        verbose_name = 'gegeven'
        verbose_name_plural = 'gegevens'

    name = models.CharField(max_length=140, unique=True)
    identification = models.CharField(default='', max_length=65)
    description = models.TextField()
    cluster = models.ForeignKey(Cluster, on_delete=models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.name

# Data information / year
class DataInfo(models.Model):
    """Data information for each year."""

    class Meta:
        ordering = ['-year']
        verbose_name = 'gegeven voor jaar'
        verbose_name_plural = 'gegevens voor jaar'

    data = models.ForeignKey(Data, on_delete=models.CASCADE, related_name='info')
    year = models.PositiveSmallIntegerField()
    information = models.TextField()
    format = models.CharField(default='', max_length=65)
    value_range = models.TextField()
    value_conditions = models.TextField()
    def __str__(self):
        return '%s (%s)' % (self.data.name, self.year)

class Use(models.Model):
    """Data consumer use types."""

    class Meta:
        verbose_name = 'gegeven gebruik'
        verbose_name_plural = 'gegevens gebruik'

    name = models.CharField(max_length=140, verbose_name="naam")
    def __str__(self):
        return "{0}: {1}".format(self.id, self.name)

class ConsequenceMissing(models.Model):
    """Data consumer consequence missing types."""
    class Meta:
        verbose_name = 'consequentie missen'
        verbose_name_plural = 'consequenties missen'

    name = models.CharField(max_length=140)
    def __str__(self):
        return "{0}: {1}".format(self.id, self.name)

@reversion.register()
class DataConsumerInfo(models.Model):
    """Data used by consumer."""

    class Meta:
        ordering = ['consumer__name', 'data__name']
        verbose_name = 'gegeven bij afnemer'
        verbose_name_plural = 'gegevens bij afnemer'
        unique_together = ("data", "consumer")

    data = models.ForeignKey(Data, on_delete=models.CASCADE, related_name='consumers', verbose_name="gegeven")
    consumer = models.ForeignKey(Consumer, on_delete=models.CASCADE, related_name='data', verbose_name="afnemer")
    use = models.ForeignKey(Use, on_delete=models.SET_NULL, blank=True, null=True, verbose_name="aanwending")
    use_text = models.TextField(default='', verbose_name="toelichting aanwending")
    consequence_missing = models.ForeignKey(ConsequenceMissing, on_delete=models.SET_NULL, blank=True, null=True, verbose_name="gevolg ontbreken")
    consequence_missing_text = models.TextField(default='', verbose_name="toelichting gevolg ontbreken")
    basis_use = models.TextField(default='', verbose_name="grondslag afname")
    def __str__(self):
        return '%s (%s)' % (self.data, self.consumer.name)

class Delivery(models.Model):
    """Consumer data delivery info."""

    class Meta:
        ordering = ['consumer__name', 'contract_type']
        verbose_name = 'levering'
        verbose_name_plural = 'lerveringen'

    consumer = models.ForeignKey(Consumer, on_delete=models.CASCADE, related_name='deliveries', verbose_name="afnemer")
    contract_type = models.CharField(max_length=140, verbose_name="contracttype")
    contract_number = models.CharField(max_length=140, verbose_name="contractnummer")
    delivery_type = models.CharField(max_length=140, verbose_name="soort levering")
    frequency = models.CharField(max_length=140, verbose_name="frequentie")

    def __str__(self):
        return '%s (%s)' % (self.contract_type, self.consumer.name)

class ConsumerUser(models.Model):
    """Consumer users."""

    class Meta:
        verbose_name = 'beheerder bij afnemer'
        verbose_name_plural = 'beheerders bij afnemer'
        unique_together = ("consumer", "user")
    
    consumer = models.ForeignKey(Consumer, on_delete=models.CASCADE, related_name='users', verbose_name="afnemer")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='consumers', verbose_name="beheerder")

    def __str__(self):
        return '%s (%s)' % (self.user.name, self.consumer.name)
    
class EventLog(models.Model):
    """Event log."""


    class Events(models.TextChoices):
        ACCESS_REQUEST_APPROVED = 'ACCESS_REQUEST_APPROVED', _('Access request approved')
        ACCESS_REQUEST_DENIED = 'ACCESS_REQUEST_DENIED', _('Access request denied')

    class Meta:
        ordering = ['-created']

    ip = models.GenericIPAddressField(null=True, blank=True, verbose_name="IP")
    userid = models.IntegerField(null=True, blank=True, default=0)
    email = models.CharField(max_length=255, null=True, blank=True)
    event_type = models.CharField(null=True, blank=True, max_length=255, choices=Events.choices)
    data = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s (%s)' % (self.event_type, self.email)
    
    def add_event(request, event_type, user=None, data=None):
        """Add event to log."""
        ip = request.session.get('ip', None)
        email = user.email if user else None
        userid = user.id if user else None
        EventLog.objects.create(ip=ip, userid=userid, email=email, event_type=event_type, data=data)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from core.models import Consumer
from core.models import Use
from core.models import Data

# Create your tests here.
class ConsumerModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # create data objects
        Data.objects.create(name="Test 1")
        Data.objects.create(name="Test 2")
        # create use object
        Use.objects.create(id=7, name="Geen gebruik")
        Use.objects.create(id=99, name="Onbekende code/aanpassen!")
        # create consumer
        Consumer.objects.create(name='Test Consumer', description='Just testing', website='http://www.test.com')

    def test_object_name(self):
        consumer = Consumer.objects.get(id=1)
        expected_object_name = 'Test Consumer'
        self.assertEqual(expected_object_name, str(consumer))

    def test_related_data(self):
        consumer = Consumer.objects.get(id=1)
        items = len(Data.objects.filter(consumers__consumer=consumer))
        expected_items = 2
        self.assertEqual(expected_items, items)

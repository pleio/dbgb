# Generated by Django 2.1.1 on 2018-12-11 08:44

from django.db import migrations, models

def update_null_values(apps, schema_editor):
    DataConsumerInfo = apps.get_model('core', 'DataConsumerInfo')
    DataConsumerInfo.objects.filter(consequence_missing_text__isnull=True).update(consequence_missing_text='')
    DataConsumerInfo.objects.filter(use_text__isnull=True).update(use_text='')
    DataConsumerInfo.objects.filter(basis_use__isnull=True).update(basis_use='')

class Migration(migrations.Migration):

    dependencies = [
        ('core', '0017_auto_20181023_1301'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dataconsumerinfo',
            name='consequence_missing_text',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='dataconsumerinfo',
            name='use_text',
            field=models.TextField(default=''),
        ),
        migrations.AlterField(
            model_name='dataconsumerinfo',
            name='basis_use',
            field=models.TextField(default=''),
        ),
        migrations.RunPython(update_null_values, reverse_code=migrations.RunPython.noop)
    ]

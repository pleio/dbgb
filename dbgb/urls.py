"""dbgb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from core import views as core_views
import pleio_auth

urlpatterns = [
    path('', include('pleio_auth.urls')),
    path('', core_views.home, name='home'),
    path('data', core_views.data, name='data'),
    path('data/<int:id>', core_views.data_detail, name='data_detail'),
    path('consumer', core_views.consumer, name='consumer'),
    path('consumer/<int:id>', core_views.consumer_detail, name='consumer_detail'),
    path('manage/consumer/<int:id>', core_views.manage_consumer, name='manage_consumer'),
    path('manage/consumer/<int:id>/data', core_views.manage_consumer_data, name='manage_consumer_data'),
    path('manage/consumer/<int:id>/data/new', core_views.manage_consumer_data_new, name='manage_consumer_data_new'),
    path('manage/consumer/<int:id>/data/<int:data_id>', core_views.manage_consumer_data_detail, name='manage_consumer_data_detail'),
    path('documents', core_views.documents, name='documents'),
    path('rules', core_views.rules, name='rules'),
    path('account', core_views.account, name='account'),
    path('admin/login/', pleio_auth.views.login, name='admin_login'),
    path('admin/', admin.site.urls)
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

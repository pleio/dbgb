#!/usr/bin/env bash

# Collect static
python manage.py collectstatic --noinput

# Run migrations
python manage.py migrate

# Start Gunicorn processes
echo "Starting development server"
python manage.py runserver 0.0.0.0:8000
# DBGB
DBGB stands for "Database Gegevensbelang". It holds information about the data attributes of the the Dutch payroll tax.

The database also keeps track which other (government) agencies make use of this data.

Every year changes could occure to these data attributes. 

The source of this data is a yearly updated "gegevensspecificatie" `docx` document. 

We developed a import tool to read data from this document, up until now (2025) the structure of this document was kept the same. 

Go to [Tools](#tools) to read about how add the new "gegevensspecificatie" for the new year.


## Setup development (through docker-compose)
Copy `.env-example` to `.env` and update the OIDC endpoint credentials you got for account.pleio-test.nl

Make sure [Docker](https://www.docker.com/) is installed. Then run the
following commands within the repository:

```sh
docker-compose up
```

This will spin up a PostgreSQL container and a Django web container. Then
create a superuser account using:

```sh
docker-compose exec web python manage.py createsuperuser
```

Now login with your new (superuser) account on http://localhost:8000/admin/

See frontend below to build or live watch frontend

## Frontend
For local development you have to build or live watch the frontend:

### Build frontend 
```
cd frontend
yarn install
yarn build
```

### Watch frontend
```
cd frontend 
yarn install
yarn dev
```

## Tools

### Import new gegevensspecificatie

Import "gegevensspecificatie" document for year. 

Usage: python manage.py importdocx <file> <year>

This managment script will read the docx document and extract data. Existing data will be updated.

```
docker-compose exec web python manage.py importdocx import/XXXX.docx 2019
```

### How to update test/prod environment

First make a backup of the core data and store the data somewhere safe.

On a production/test pod run the following command:

```
python manage.py dumpdata core > /tmp/core.json
```

Copy the data locally with kubectl:

```sh
kubectl cp -n dbgb <pod>:/tmp/core.json import/<date>-core.json
```

Now we will import the new data from the docx document.

Copy the docx document to the pod:

```sh
kubectl cp -n dbgb import/XXX.docx <pod>:/tmp/xxx.docx
```

Run the management script on the pod. Make sure your running the import on the correct year!
```sh
python manage.py importdocx import/XXXX.docx 2022
```

### How to restore data when something goes wrong (or you want to test with the data locally)
```
python manage.py migrate core zero
python manage.py migrate core
python manage.py loaddata import/XXXXX-core.json
```

If you get a error locally that a user does not exist, you can remove the core.consumeruser data in the json file:

```json
,
    {
        "model": "core.consumeruser",
        "pk": 6,
        "fields": {
            "consumer": 20,
            "user": 4
        }
    }
```

### Make a user admin from the shell
```sh
python manage.py shell
from pleio_auth.models import User
user = Users.object.get(email='xxxx')
user.is_admin = True
user.is_superuser = True
user.save()
```

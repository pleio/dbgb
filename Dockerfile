FROM node:12-alpine as frontend

WORKDIR /app

# Install yarn and other dependencies via apk

COPY ./frontend/package.json ./frontend/yarn.lock /app/
COPY ./frontend/webpack.base.config.js ./frontend/webpack.prod.config.js /app/
COPY ./frontend/postcss.config.js /app/
COPY ./frontend/.babelrc /app/

RUN yarn install

ADD frontend/static /app/static/
ADD frontend/src /app/src/

RUN yarn run build

FROM python:3.11-slim AS backend

RUN apt-get update && apt-get install --no-install-recommends -y \
    build-essential \
    libpq-dev

RUN python -m venv /app-tmp/venv && /app-tmp/venv/bin/pip install --upgrade pip

WORKDIR /app-tmp
COPY requirements.txt /app-tmp
RUN /app-tmp/venv/bin/pip3 install -r requirements.txt

FROM python:3.11-slim

# Workaround for error with postgresql-client package
RUN mkdir -p /usr/share/man/man1/ /usr/share/man/man3/ /usr/share/man/man7/

RUN apt-get update && apt-get install --no-install-recommends -y \
    gettext \
    git \
    mime-support \
    libmagic-dev \
    libpq-dev

COPY --from=backend /app-tmp/venv /app-tmp/venv
ENV PATH="/app-tmp/venv/bin:${PATH}"

WORKDIR /app

# Add app
ADD . /app

# Copy frontend code from previous stage
COPY --from=frontend /app/static/bundles /app/frontend/static/bundles
COPY --from=frontend /app/webpack-stats.json /app/frontend/webpack-stats.json

# Create media and static folders
RUN mkdir -p /app/media /app/static && chown www-data:www-data /app/media /app/static

# Boot script
ADD docker/start.sh docker/start-dev.sh /
RUN chmod +x /start.sh /start-dev.sh

USER www-data

# HTTP port
EXPOSE 8000

ENV PYTHONUNBUFFERED 1

# Define run script
CMD ["/start.sh"]

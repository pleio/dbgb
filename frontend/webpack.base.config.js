const {
	VueLoaderPlugin
} = require('vue-loader');

const BundleTracker = require("webpack-bundle-tracker");
const path = require("path");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
	entry: {
		app: [
			'./src/app.js'
		]
	},
	output: {
		path: path.resolve(__dirname, "static/bundles"),
		filename: "[name].[contenthash:8].js",
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				use: {
					loader: 'babel-loader',
				}
			},
			{
				test: /\.vue$/,
				use: 'vue-loader'
			},
			{
				test: /\.css$|.scss$/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					'sass-loader',
					'postcss-loader',
				]
			},
			{
				test: /\.(ttf|eot|woff)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							esModule: false,
							name: './fonts/[name].[ext]?[contenthash:8]',
						}
					}
				]
			},
		]
	},
	plugins: [
		new VueLoaderPlugin(),
		new BundleTracker({ filename: './webpack-stats.json' }),
		new MiniCssExtractPlugin({
			filename: 'app.[contenthash:8].css',
		}),
	]
}

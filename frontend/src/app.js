import './scss/main.scss'
import 'bootstrap'
// import Vue from 'vue'
// import App from './App.vue'

/**
 * Add d-update-image="img-id" attribute to file input to update img src.
 */
const exec_update_image = document.querySelectorAll('[d-update-image]');

for (let el of exec_update_image) {

	if (el.type === 'file') {
		let target = el.getAttribute('d-update-image');

		el.addEventListener('change', function (e) {
			var selectedFile = event.target.files[0];
			var reader = new FileReader();

			var imgtag = document.getElementById(target);
			imgtag.title = selectedFile.name;

			reader.onload = function (event) {
				imgtag.src = event.target.result;
			};

			reader.readAsDataURL(selectedFile);
		});
	}
}

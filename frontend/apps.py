from . import settings
from django.apps import AppConfig

class FrontendConfig(AppConfig):
    name = 'frontend'

const merge = require('webpack-merge')
const webpackBaseConfig = require('./webpack.base.config.js')

module.exports = merge(webpackBaseConfig, {
  output: {
    publicPath: "http://localhost:9002/static/bundles/"
  },
  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    port: 9002
  },
})

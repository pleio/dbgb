from typing import Any
from django.contrib import admin
from django.db.models.query import QuerySet
from django.http.request import HttpRequest
from .models import User, AccessRequest
from core.models import EventLog
from django.db import IntegrityError
from django.conf import settings
from django.contrib.admin import SimpleListFilter
from django.template.loader import render_to_string
from django.core.mail import send_mail
from email.utils import formataddr
from import_export.admin import ExportMixin
from import_export import resources

def accept(modeladmin, request, queryset):
    for access_request in queryset:
        try:
            User.objects.create_user(
                email = access_request.email,
                name = access_request.name,
                password = None,
                external_id = access_request.external_id
            )

            EventLog.add_event(request, EventLog.Events.ACCESS_REQUEST_APPROVED, user=request.user, data=access_request.email)

            send_mail(
                render_to_string('emails/access_granted_subject.txt', { 'access_request': access_request, 'host': request.get_host() }),
                render_to_string('emails/access_granted.txt', { 'access_request': access_request, 'host': request.get_host(), 'url': request.build_absolute_uri('/') }),
                settings.EMAIL_FROM,
                [formataddr((access_request.name, access_request.email))],
                fail_silently=True
            )

        except IntegrityError:
            pass

        access_request.delete()

accept.short_description = "Accept this access request"

class EmailDomainFilter(SimpleListFilter):
    title = 'email domein'
    parameter_name = 'email_domain'

    def lookups(self, request, model_admin):
        # lookup all unique email domains after @ from users model
        domains = {email.split('@')[1] for email in User.objects.values_list('email', flat=True).distinct()}
        sorted_domains = sorted(domains)  # Sort the domains in ascending order

        return [(domain, domain) for domain in sorted_domains]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(email__endswith=self.value())



class LastLoginFilter(SimpleListFilter):
    title = 'laatste aanmelding'
    parameter_name = 'last_login'

    def lookups(self, request, model_admin):
        # lookup distinct years from last_login field from users model
        years = {date.year for date in User.objects.values_list('last_login', flat=True).distinct() if date}
        years = sorted(years)  # Sort the years in ascending order
        result = [(year, year) for year in years]
        result.append(('Null', 'Nooit'))
        return result

    def queryset(self, request, queryset):
        if self.value() == 'Null':
            return queryset.filter(last_login__isnull=True)
        elif self.value():
            return queryset.filter(last_login__year=self.value())


class AccessRequestAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    readonly_fields = ('created_at', 'name', 'email', 'external_id')
    list_display = ('name', 'email', 'created_at')
    actions = [accept]

    def delete_model(self, request, obj):
        EventLog.add_event(request, EventLog.Events.ACCESS_REQUEST_DENIED, user=request.user, data=obj.email)
        super().delete_model(request, obj)

    def delete_queryset(self, request: HttpRequest, queryset: QuerySet[Any]) -> None:
        """Delete all access requests and log the event."""
        for access_request in queryset:
            EventLog.add_event(request, EventLog.Events.ACCESS_REQUEST_DENIED, user=request.user, data=access_request.email)
        return super().delete_queryset(request, queryset)


class UserAdminResouce(resources.ModelResource):
    class Meta:
        model = User
        fields = ('id', 'email', 'name', 'last_login')
        export_order = ('id', 'email', 'name', 'last_login')


class UserAdmin(ExportMixin, admin.ModelAdmin):
    def has_add_permission(self, request):
        return False
    
    resource_class = UserAdminResouce
    readonly_fields = ('external_id', 'last_login')
    list_display = ('name', 'email', 'is_active', 'last_login')
    list_filter = (EmailDomainFilter, LastLoginFilter,)
    exclude = ('password', )


admin.site.register(User, UserAdmin)

if settings.PLEIO_OAUTH_REQUEST_ACCESS:
    admin.site.register(AccessRequest, AccessRequestAdmin)
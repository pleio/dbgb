{{/*
Expand the name of the chart.
*/}}
{{- define "dbgb.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end }}

{{- define "dbgb.initName" -}}
{{- printf "%s-init" ((include "dbgb.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "dbgb.secretsName" -}}
{{- printf "%s-secrets" ((include "dbgb.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "dbgb.tlsSecretName" -}}
{{- printf "tls-%s" ((include "dbgb.name" .)) | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "dbgb.storageName" -}}
{{- if .Values.storage.existingDataStorage }}
{{- .Values.storage.existingDataStorage -}}
{{- else }}
{{- printf "%s-pvc" ((include "dbgb.name" .)) -}}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "dbgb.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "dbgb.labels" -}}
helm.sh/chart: {{ include "dbgb.chart" . }}
{{ include "dbgb.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "dbgb.selectorLabels" -}}
app.kubernetes.io/name: {{ include "dbgb.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

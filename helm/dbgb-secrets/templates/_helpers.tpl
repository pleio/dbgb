{{- define "dbgb.name" -}}
{{- .Release.Name | trunc 63  -}}
{{- end -}}

{{- define "dbgb.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "dbgb.labels" -}}
helm.sh/chart: {{ include "dbgb.chart" . }}
{{ include "dbgb.selectorLabels" . }}
{{- end -}}

{{- define "dbgb.selectorLabels" -}}
app.kubernetes.io/name: {{ include "dbgb.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}
